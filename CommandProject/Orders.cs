﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommandProject.DataBaseEntities;

namespace CommandProject
{
    public partial class Orders : Form
    {
        Context db;
        public Orders()
        {
            InitializeComponent();
            db = new Context();
            db.Orders.Load();
            db.Subjects.Load();
            db.TaskTypes.Load();
            db.Clients.Load();
            db.Authors.Load();
            db.OrderStatuses.Load();
            db.Specialisations.Load();
            dataGridView1.AutoGenerateColumns = false;

            comboBox1.DataSource = db.TaskTypes.Local.ToBindingList();
            comboBox2.DataSource = db.OrderStatuses.Local.ToBindingList();
            comboBox3.DataSource = db.Authors.Local.ToBindingList();
            comboBox4.DataSource = db.Clients.Local.ToBindingList();
            comboBox5.DataSource = db.Subjects.Local.ToBindingList();

            comboBox1.DisplayMember = "Title";
            comboBox2.DisplayMember = "Title";
            comboBox3.DisplayMember = "WholeName";
            comboBox4.DisplayMember = "WholeName";
            comboBox5.DisplayMember = "Title";


            dataGridView1.DataSource = db.Orders.Local.ToBindingList();
        }

        private void Orders_Load(object sender, EventArgs e)
        {
            ResetFields();
        }

        private void ResetFields()
        {
            textBox4.Text = "";
            dateTimePicker1.Value = DateTime.Today;
            dateTimePicker2.Value = DateTime.Today;
            comboBox1.Text = "";
            comboBox2.Text = "";
            comboBox3.Text = "";
            comboBox4.Text = "";
            comboBox5.Text = "";
            numericUpDown1.Value = 0;
            numericUpDown2.Value = 0;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonDetails_Click(object sender, EventArgs e)
        {

            if (dataGridView1.SelectedRows.Count > 0)
            {
                Order order = dataGridView1.SelectedRows[0].DataBoundItem as Order;
                (new OrderDetails(order, db)).ShowDialog();
            }
        }

        private void buttonOrderAdd_Click(object sender, EventArgs e)
        {
            OrderCreateUpdate dialog = new OrderCreateUpdate();
            dialog.comboBox1.DataSource = db.Clients.Local.ToBindingList();
            dialog.comboBox1.DisplayMember = "WholeName";
            dialog.comboBox2.DataSource = db.OrderStatuses.Local.ToBindingList();
            dialog.comboBox2.DisplayMember = "Title";
            dialog.comboBox3.DataSource = db.Specialisations.Local.ToBindingList();
            dialog.comboBox3.DisplayMember = "WholeSpecialization";

            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.Cancel)
                return;

            Order order = new Order();
            order.Client = dialog.comboBox1.SelectedItem as Client;
            order.CompletionDate = dialog.dateTimePicker1.Value;
            order.StartDate = dialog.dateTimePicker2.Value;
            order.EndDate = dialog.dateTimePicker3.Value;
            order.OrderStatus = dialog.comboBox2.SelectedItem as OrderStatus;
            order.Number = dialog.textBoxName.Text;
            order.Price = Int32.Parse(dialog.textBox10.Text);
            order.AuthorSalary = Int32.Parse(dialog.textBox11.Text);
            order.Specialisation = dialog.comboBox3.SelectedItem as Specialisation;
            order.Task = dialog.textBox1.Text;
            


            db.Orders.Add(order);
            db.SaveChanges();
            dataGridView1.Refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Order order = dataGridView1.SelectedRows[0].DataBoundItem as Order;
                OrderCreateUpdate dialog = new OrderCreateUpdate();
                dialog.comboBox1.DataSource = db.Clients.Local.ToBindingList();
                dialog.comboBox1.DisplayMember = "WholeName";
                dialog.comboBox2.DataSource = db.OrderStatuses.Local.ToBindingList();
                dialog.comboBox2.DisplayMember = "Title";
                dialog.comboBox3.DataSource = db.Specialisations.Local.ToBindingList();
                dialog.comboBox3.DisplayMember = "WholeSpecialization";

                dialog.comboBox1.SelectedItem = order.Client;
                dialog.comboBox2.SelectedItem = order.OrderStatus;
                dialog.comboBox3.SelectedItem = order.Specialisation;
                dialog.dateTimePicker1.Value = order.CompletionDate;
                dialog.dateTimePicker2.Value = order.StartDate;
                dialog.dateTimePicker3.Value = order.EndDate;
                dialog.textBox1.Text = order.Task;
                dialog.textBox10.Text = order.Price.ToString();
                dialog.textBox11.Text = order.AuthorSalary.ToString();
                dialog.textBoxName.Text = order.Number;

                DialogResult result = dialog.ShowDialog();

                if (result == DialogResult.Cancel)
                    return;

                order.Client = dialog.comboBox1.SelectedItem as Client;
                order.CompletionDate = dialog.dateTimePicker1.Value;
                order.StartDate = dialog.dateTimePicker2.Value;
                order.EndDate = dialog.dateTimePicker3.Value;
                order.OrderStatus = dialog.comboBox2.SelectedItem as OrderStatus;
                order.Number = dialog.textBoxName.Text;
                order.Price = Int32.Parse(dialog.textBox10.Text);
                order.AuthorSalary = Int32.Parse(dialog.textBox11.Text);
                order.Specialisation = dialog.comboBox3.SelectedItem as Specialisation;
                order.Task = dialog.textBox1.Text;

                db.SaveChanges();
                dataGridView1.Refresh();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Order order = dataGridView1.SelectedRows[0].DataBoundItem as Order;
                db.Orders.Remove(order);
                db.SaveChanges();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var query = db.Orders.Local.Where
            (
                o =>
                (textBox4.Text.Length == 0 || textBox4.Text == o.Number) &&
                (comboBox2.Text.Length == 0 || comboBox2.SelectedItem == o.OrderStatus) &&
                (comboBox4.Text.Length == 0 || comboBox4.SelectedItem == o.Client) &&
                !(o.EndDate < dateTimePicker1.Value) &&
                !(o.StartDate > dateTimePicker2.Value) &&
                (numericUpDown2.Value == 0 || o.Price <= numericUpDown2.Value) &&
                o.Price >= numericUpDown1.Value &&
                (comboBox5.Text.Length == 0 || comboBox5.SelectedItem == o.Specialisation.Subject) &&
                (comboBox1.Text.Length == 0 || comboBox1.SelectedItem == o.Specialisation.TaskType) &&
                (comboBox3.Text.Length == 0 || comboBox3.SelectedItem == o.Specialisation.Author)
            );


            dataGridView1.DataSource = query.ToList();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ResetFields();
            dataGridView1.DataSource = db.Orders.Local.ToBindingList();
        }
    }
}
