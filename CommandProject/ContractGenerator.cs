﻿using CommandProject.DataBaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.IO;
using Novacode;

namespace CommandProject
{
    class ContractGenerator
    {
        public static bool isValidOrder(Order order)
        {
            bool res = true;

            if (order.Number.Equals(""))
                res = false;

            if (order.StartDate == null || order.EndDate == null)
                res = false;

            if (order.Task.Equals(""))
                res = false;

            if (order.Client == null)
                res = false;

            if (order.Specialisation == null)
                res = false;

            return res;
        }

        public static void generateContractWithClient(Order order)
        {
            if (!Directory.Exists("contracts"))
            {
                Directory.CreateDirectory("contracts");
            }

            using (DocX document = DocX.Create(@"contracts/contractClient" + order.Number + @".docx"))
            {
                Paragraph p = document.InsertParagraph();

                p.Append("\nДоговор возмездного оказания услуг №" + order.Number)
                .FontSize(14)
                .Bold().Alignment = Alignment.center;

                p = document.InsertParagraph();
                p.Append("\n"+order.StartDate.ToShortDateString() + "\n")
                .Alignment = Alignment.right;

                p = document.InsertParagraph();
                p.Append("ООО «Рога и копыта» именуемый в дальнейшем «Исполнитель», с одной стороны, и " + order.Client.WholeName + " именуемый в дальнейшем «Заказчик», с другой стороны, вместе именуемые «Стороны», заключили настоящий Договор о нижеследующем:");

                p = document.InsertParagraph();
                p.Append("Исполнитель обязуется выполнить " + order.Specialisation.TaskType.Title + " по предмету " + order.Specialisation.Subject.Title + " в срок до " + order.EndDate.ToShortDateString() + " в соответствии с заданием: ");

                p = document.InsertParagraph();
                p.Append(order.Task + "\n");

                p = document.InsertParagraph();
                p.Append("Стоимость услуг по настоящему Договору составляет " + order.Price + " рублей.");

                p = document.InsertParagraph();
                p.Append("Заказчик обязуется оплатить работы путем внесения предоплаты в размере 100% от суммы договора.\n");

                p = document.InsertParagraph();
                p.Append("В случае ненадлежащего качества работ, исполнитель обязуется исправить все недостатки либо осуществить возврат денег.");

                p = document.InsertParagraph();
                p.Append("В случае невыполнения работ срок, исполнитель обязуется вернуть деньги заказчику по первому требованию*.\n");

                p = document.InsertParagraph();
                p.Append("Подпись заказчика ___________").Alignment = Alignment.right;

                p = document.InsertParagraph();
                p.Append("Подпись исполнителя ___________").Alignment = Alignment.right;

                document.Save();
            }
        }

        public static void generateContractWithAuthor(Order order)
        {
            //HelloWorld();
            if (!Directory.Exists("contracts"))
            {
                Directory.CreateDirectory("contracts");
            }

            using (DocX document = DocX.Create(@"contracts/contractAuthor" + order.Number + @".docx"))
            {
                Paragraph p = document.InsertParagraph();

                p.Append("\nДоговор возмездного оказания услуг №" + order.Number)
                .FontSize(14)
                .Bold().Alignment = Alignment.center;

                p = document.InsertParagraph();
                p.Append("\n" + order.StartDate.ToShortDateString() + "\n")
                .Alignment = Alignment.right;

                p = document.InsertParagraph();
                p.Append(order.Specialisation.Author.WholeName + " именуемый в дальнейшем «Исполнитель», с одной стороны, и ООО «Рога и копыта»  именуемый в дальнейшем «Заказчик», с другой стороны, вместе именуемые «Стороны», заключили настоящий Договор о нижеследующем:");

                p = document.InsertParagraph();
                p.Append("Исполнитель обязуется выполнить " + order.Specialisation.TaskType.Title + " по предмету " + order.Specialisation.Subject.Title + " в срок до " + order.EndDate.ToShortDateString() + " в соответствии с заданием: ");

                p = document.InsertParagraph();
                p.Append(order.Task + "\n");

                p = document.InsertParagraph();
                p.Append("Стоимость услуг по настоящему Договору составляет " + order.AuthorSalary + " рублей.");

                p = document.InsertParagraph();
                p.Append("Заказчик обязуется оплатить работы по факту успешного выполнения задания в срок.\n");

                p = document.InsertParagraph();
                p.Append("В случае ненадлежащего качества работ, исполнитель обязуется исправить все недостатки.");

                p = document.InsertParagraph();
                p.Append("В случае невыполнения работ срок, исполнитель может быть добавлен заказчиком в черный список по своему усмотрению.\n");

                p = document.InsertParagraph();
                p.Append("Подпись заказчика ___________").Alignment = Alignment.right;

                p = document.InsertParagraph();
                p.Append("Подпись исполнителя ___________").Alignment = Alignment.right;

                document.Save();
            }
        }
    }
}
