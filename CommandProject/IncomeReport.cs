﻿using CommandProject.DataBaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CommandProject
{
    public partial class IncomeReport : Form
    {
        DateTime fromDate, toDate;
        const int ORDER_COMPLETE_KEY = 1;
        List<DataBindingProjection> data;

        private class DataBindingProjection
        {
            public string OrderNumber { get; set; }
            public int Income { get; set; }
            public int AuthorSalary { get; set; }
            public int RefundCost
            {
                get
                {
                    if (Refund == null)
                        return 0;
                    return Refund.Cost;
                }
            }
            public DateTime Date { get; set; }

            public Refund Refund;
        }

        public IncomeReport()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            fromDate = dateTimePicker1.Value;
            toDate = dateTimePicker2.Value;
            RefreshTable();
        }

        private void RefreshTable()
        {
            if (fromDate == null || toDate == null)
            {
                data = new List<DataBindingProjection>();
                return;
            }

            using (var db = new Context())
            {
                var query = from ord in db.Orders
                            where ord.OrderStatus.Id == ORDER_COMPLETE_KEY && ord.CompletionDate <= toDate && ord.CompletionDate >= fromDate
                            select new DataBindingProjection
                                { OrderNumber = ord.Number, Income = ord.Price, AuthorSalary = ord.AuthorSalary, Refund = ord.RefundReason, Date = ord.CompletionDate };
                data = query.ToList();
                dataGridView1.DataSource = data;
                CalculateAndShowResults();
                RefreshTableHeaders();
            }
        }

        private void CalculateAndShowResults()
        {
            int income = 0, salary = 0, refund = 0, gain;
            foreach(var row in data)
            {
                income += row.Income;
                salary += row.AuthorSalary;
                refund += row.RefundCost;
            }
            gain = income - salary - refund;
            label8.Text = income.ToString() + " руб";
            label9.Text = salary.ToString() + " руб";
            label10.Text = refund.ToString() + " руб";
            label11.Text = gain.ToString() + " руб";

        }

        private void RefreshTableHeaders()
        {
            dataGridView1.Columns[0].HeaderText = "Номер заказа";
            dataGridView1.Columns[1].HeaderText = "Доход";
            dataGridView1.Columns[2].HeaderText = "Зарплата автора";
            dataGridView1.Columns[3].HeaderText = "Возврат денег";
            dataGridView1.Columns[4].HeaderText = "Дата";
        }
    }
}
