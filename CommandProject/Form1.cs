﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CommandProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void отчетыToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void авторыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new Authors()).Show();
        }

        private void репетиторыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new Clients()).Show();
        }

        private void предметыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new Subjects()).Show();
        }

        private void отчетыToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
        }

        private void заказыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new Orders()).Show();
        }

        private void отчетОДоходахToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new IncomeReport()).Show();
        }

        private void отчетОРаботахАвторовToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new AuthorsResultsReport()).Show();
        }

        private void типыРаботToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new TaskTypes()).Show();
        }

        private void квалификацииToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new Qualifications()).Show();
        }

        private void статусыЗаказовToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new OrderStatuses()).Show();
        }
    }
}
