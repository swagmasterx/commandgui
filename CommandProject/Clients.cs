﻿using CommandProject.DataBaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CommandProject
{
    public partial class Clients : Form
    {
        public Context db;
        public List<Client> data;
        public Clients()
        {
            InitializeComponent();
            LoadDatabase();
        }

        private void LoadDatabase()
        {
            db = new Context();
            PerformSearch();
        }

        private void PerformSearch()
        {
            var query = from cl in db.Clients
                        where (textBox1.Text.Length == 0 || textBox1.Text == cl.Name) &&
                        (textBox2.Text.Length == 0 || textBox2.Text == cl.Surname) &&
                        (textBox3.Text.Length == 0 || textBox3.Text == cl.Fathername) &&
                        (textBox4.Text.Length == 0 || textBox4.Text == cl.PhoneNumber) &&
                        (textBox5.Text.Length == 0 || textBox5.Text == cl.Email) &&
                        checkBox1.Checked == cl.Blocked
                        select cl;
            data = query.ToList();
            dataGridView1.DataSource = data;

            ChangeTableView();
        }

        private void ChangeTableView()
        {
            dataGridView1.Columns["Id"].Visible = false;
            dataGridView1.Columns["Orders"].Visible = false;
            dataGridView1.Columns["Name"].HeaderText = "Имя";
            dataGridView1.Columns["Surname"].HeaderText = "Фамилия";
            dataGridView1.Columns["Fathername"].HeaderText = "Отчество";
            dataGridView1.Columns["PhoneNumber"].HeaderText = "Номер телефона";
            dataGridView1.Columns["Blocked"].HeaderText = "Заблокирован";

        }

        private void buttonClientAdd_Click(object sender, EventArgs e)
        {

            ClientCreateUpdate dialog = new ClientCreateUpdate();
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.Cancel)
                return;

            db.Clients.Add(dialog.Client);
            db.SaveChanges();

            PerformSearch();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonClientEdit_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
                return;

            int idx = dataGridView1.SelectedRows[0].Index;
            ClientCreateUpdate dialog = new ClientCreateUpdate(data[idx]);
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.Cancel)
                return;

            db.SaveChanges();

            PerformSearch();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PerformSearch();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            checkBox1.Checked = false;
            PerformSearch();
        }

        private void buttonClientDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
                return;

            int idx = dataGridView1.SelectedRows[0].Index;
            Client toDelete = data[idx];
            db.Clients.Remove(toDelete);
            db.SaveChanges();

            PerformSearch();
        }
    }
}
