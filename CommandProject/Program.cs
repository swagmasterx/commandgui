﻿using CommandProject.DataBaseEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CommandProject
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            /*using (var db = new Context())
            {
                ReworkReason reason = new ReworkReason{ Title = "Несоответствие требованиям."};
                db.ReworkReasons.Add(reason);
                db.SaveChanges();

                var reasons = db.ReworkReasons.ToList();
                Console.WriteLine("Данные после добавления:");
                foreach (ReworkReason u in reasons)
                {
                    Console.WriteLine($"{u.Id}; {u.Title}");
                }
            }*/

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            
        }
    }
}
