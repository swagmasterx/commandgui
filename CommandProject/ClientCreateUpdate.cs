﻿using CommandProject.DataBaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CommandProject
{
    public partial class ClientCreateUpdate : Form
    {
        public Client Client;

        public ClientCreateUpdate()
        {
            InitializeComponent();
            Client = new Client() { };
        }

        public ClientCreateUpdate(Client Client)
        {
            InitializeComponent();
            this.Client = Client;
            textBox1.Text = Client.Name;
            textBox2.Text = Client.Surname;
            textBox3.Text = Client.Fathername;
            textBox4.Text = Client.Email;
            textBox6.Text = Client.PhoneNumber;
            checkBox1.Checked = Client.Blocked;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (TryFillFields())
            {
                MessageBox.Show("Запись сохранена.");
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Заполните все данные!");
            }
        }

        private bool TryFillFields()
        {
            if (FieldsFilled())
            {
                Client.Name = textBox1.Text;
                Client.Surname = textBox2.Text;
                Client.Fathername = textBox3.Text;
                Client.Email = textBox4.Text;
                Client.PhoneNumber = textBox6.Text;
                Client.Blocked = checkBox1.Checked;
                return true;
            }
            return false;
        }

        private bool FieldsFilled()
        {
            return textBox1.Text.Length > 0 &&
                textBox2.Text.Length > 0 &&
                textBox3.Text.Length > 0 &&
                textBox4.Text.Length > 0 &&
                textBox6.Text.Length > 0;
        }
    }
}
