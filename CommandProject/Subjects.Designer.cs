﻿namespace CommandProject
{
    partial class Subjects
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonSubjectDelete = new System.Windows.Forms.Button();
            this.buttonSubjectEdit = new System.Windows.Forms.Button();
            this.buttonSubjectAdd = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.справочникиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.авторыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.репетиторыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.предметыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.заказыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnName});
            this.dataGridView1.Location = new System.Drawing.Point(12, 27);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(258, 150);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // buttonSubjectDelete
            // 
            this.buttonSubjectDelete.Location = new System.Drawing.Point(195, 183);
            this.buttonSubjectDelete.Name = "buttonSubjectDelete";
            this.buttonSubjectDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonSubjectDelete.TabIndex = 41;
            this.buttonSubjectDelete.Text = "Удалить";
            this.buttonSubjectDelete.UseVisualStyleBackColor = true;
            this.buttonSubjectDelete.Click += new System.EventHandler(this.buttonSubjectDelete_Click);
            // 
            // buttonSubjectEdit
            // 
            this.buttonSubjectEdit.Location = new System.Drawing.Point(93, 183);
            this.buttonSubjectEdit.Name = "buttonSubjectEdit";
            this.buttonSubjectEdit.Size = new System.Drawing.Size(96, 23);
            this.buttonSubjectEdit.TabIndex = 40;
            this.buttonSubjectEdit.Text = "Редактировать";
            this.buttonSubjectEdit.UseVisualStyleBackColor = true;
            this.buttonSubjectEdit.Click += new System.EventHandler(this.buttonSubjectEdit_Click);
            // 
            // buttonSubjectAdd
            // 
            this.buttonSubjectAdd.Location = new System.Drawing.Point(12, 183);
            this.buttonSubjectAdd.Name = "buttonSubjectAdd";
            this.buttonSubjectAdd.Size = new System.Drawing.Size(75, 23);
            this.buttonSubjectAdd.TabIndex = 39;
            this.buttonSubjectAdd.Text = "Добавить";
            this.buttonSubjectAdd.UseVisualStyleBackColor = true;
            this.buttonSubjectAdd.Click += new System.EventHandler(this.buttonSubjectAdd_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.справочникиToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(287, 24);
            this.menuStrip1.TabIndex = 42;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // справочникиToolStripMenuItem
            // 
            this.справочникиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.авторыToolStripMenuItem,
            this.репетиторыToolStripMenuItem,
            this.предметыToolStripMenuItem,
            this.отчетыToolStripMenuItem,
            this.заказыToolStripMenuItem});
            this.справочникиToolStripMenuItem.Name = "справочникиToolStripMenuItem";
            this.справочникиToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.справочникиToolStripMenuItem.Text = "Навигация";
            // 
            // авторыToolStripMenuItem
            // 
            this.авторыToolStripMenuItem.Name = "авторыToolStripMenuItem";
            this.авторыToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.авторыToolStripMenuItem.Text = "Авторы";
            // 
            // репетиторыToolStripMenuItem
            // 
            this.репетиторыToolStripMenuItem.Name = "репетиторыToolStripMenuItem";
            this.репетиторыToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.репетиторыToolStripMenuItem.Text = "Клиенты";
            // 
            // предметыToolStripMenuItem
            // 
            this.предметыToolStripMenuItem.Name = "предметыToolStripMenuItem";
            this.предметыToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.предметыToolStripMenuItem.Text = "Области знания";
            // 
            // отчетыToolStripMenuItem
            // 
            this.отчетыToolStripMenuItem.Name = "отчетыToolStripMenuItem";
            this.отчетыToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.отчетыToolStripMenuItem.Text = "Отчеты";
            // 
            // заказыToolStripMenuItem
            // 
            this.заказыToolStripMenuItem.Name = "заказыToolStripMenuItem";
            this.заказыToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.заказыToolStripMenuItem.Text = "Заказы";
            // 
            // ColumnName
            // 
            this.ColumnName.DataPropertyName = "Title";
            this.ColumnName.HeaderText = "Название";
            this.ColumnName.Name = "ColumnName";
            this.ColumnName.Width = 200;
            // 
            // Subjects
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 226);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.buttonSubjectDelete);
            this.Controls.Add(this.buttonSubjectEdit);
            this.Controls.Add(this.buttonSubjectAdd);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Subjects";
            this.Text = "Предметы";
            this.Load += new System.EventHandler(this.Subjects_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonSubjectDelete;
        private System.Windows.Forms.Button buttonSubjectEdit;
        private System.Windows.Forms.Button buttonSubjectAdd;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem справочникиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem авторыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem репетиторыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem предметыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчетыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem заказыToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
    }
}