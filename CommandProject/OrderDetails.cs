﻿using CommandProject.DataBaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommandProject.DataBaseEntities;
using System.Data.Entity;

namespace CommandProject
{
    public partial class OrderDetails : Form
    {
        private class DataBindingProjection
        {

            public string Reason { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
        }


        Order Order;
        List<DataBindingProjection> data;
        Context db;

        public OrderDetails(Order order, Context db)
        {
            InitializeComponent();
            textBox1.Multiline = true;
            this.Order = order;
            this.db = db;
            LoadOrderInfo();
        }

        private void LoadOrderInfo()
        {
            textBox0.Text = Order.Number;
            textBox1.Text = Order.Task;
            textBox2.Text = Order.OrderStatus.Title;
            textBox3.Text = FullName(Order.Client);
            textBox4.Text = FullName(Order.Specialisation.Author);
            textBox5.Text = TextFrom(Order.Specialisation.SubjectTitle);
            textBox6.Text = Order.Specialisation.TaskTypeTitle;
            textBox11.Text = Order.AuthorSalary.ToString();
            textBox10.Text = Order.Price.ToString();

            textBox7.Text = Order.StartDate.ToString();
            textBox8.Text = Order.EndDate.ToString();
            textBox9.Text = Order.CompletionDate.ToString();

            LoadRefundInfo();
            LoadReworksInfo();
            RefreshTableHeaders();
        }

        private void LoadRefundInfo()
        {
            button1.Enabled = true;
            if (Order.RefundReason == null)
            {
                textBox13.Text = "Возврата не было";
                textBox12.Text = "-";
                textBox14.Text = "-";
            }
            else
            {
                textBox13.Text = Order.RefundReason.Reason;
                textBox12.Text = Order.RefundReason.Date.ToString();
                textBox14.Text = Order.RefundReason.Cost.ToString();
                button1.Enabled = false;
            }
        }

        private void RefreshTableHeaders()
        {
            dataGridView1.Columns["StartDate"].HeaderText = "Дата начала";
            dataGridView1.Columns["EndDate"].HeaderText = "Дата окончания";
            dataGridView1.Columns["Reason"].HeaderText = "Причина";
        }

        private void LoadReworksInfo()
        {
            data = new List<DataBindingProjection>();
            foreach(var rw in Order.Reworks)
            {
                data.Add(new DataBindingProjection {StartDate = rw.StartDate, EndDate = rw.EndDate, Reason = rw.ReworkReason.Title });
            }
            dataGridView1.DataSource = data;
        }

        private string TextFrom(Object obj)
        {
            if (obj == null)
                return "";
            return obj.ToString();
        }

        private string FullName(Client person)
        {
            return person.Name + " " + person.Surname + " " + person.Fathername;
        }

        private string FullName(Author person)
        {
            return person.Name + " " + person.Surname + " " + person.Fathername;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RefundDialog dialog = new RefundDialog();
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.Cancel)
                return;


            Refund refund = new Refund { Reason = dialog.Reason, Cost = dialog.Cost, Date = dialog.Date, Order = this.Order };
            db.Refunds.Add(refund);
            db.SaveChanges();

            MessageBox.Show("Запись сохранена!");

            LoadRefundInfo();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            ReworkDialog dialog = new ReworkDialog(db);
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.Cancel)
                return;


            Rework refund = new Rework { StartDate = dialog.fromDate, EndDate = dialog.toDate, ReworkReason = dialog.Reason, Order = this.Order };
            db.Reworks.Add(refund);
            db.SaveChanges();

            MessageBox.Show("Запись сохранена!");

            LoadReworksInfo();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (ContractGenerator.isValidOrder(Order))
            {
                ContractGenerator.generateContractWithClient(Order);

                string filePath = @"contracts/contractClient" + Order.Number + @".docx";
                MessageBox.Show("Договор сгенерирован (" + filePath + ")");
            }
            else
            {
                MessageBox.Show("Договор недозаполнен.");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (ContractGenerator.isValidOrder(Order))
            {
                ContractGenerator.generateContractWithAuthor(Order);

                string filePath = @"contracts/contractAuthor" + Order.Number + @".docx";
                MessageBox.Show("Договор сгенерирован (" + filePath + ")");
            }
            else
            {
                MessageBox.Show("Договор недозаполнен.");
            }
        }
    }
}
