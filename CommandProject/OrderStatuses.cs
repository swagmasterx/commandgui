﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommandProject.DataBaseEntities;

namespace CommandProject
{
    public partial class OrderStatuses : Form
    {
        Context db;
        public OrderStatuses()
        {
            InitializeComponent();

            db = new Context();
            
            db.OrderStatuses.Load();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = db.OrderStatuses.Local.ToBindingList();
        }

        private void buttonOrderStatusAdd_Click(object sender, EventArgs e)
        {
            OrderStatusCreateUpdate dialog = new OrderStatusCreateUpdate();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.Cancel)
                return;

            OrderStatus orderStatus = new OrderStatus();
            orderStatus.Title = dialog.textBoxName.Text;

            db.OrderStatuses.Add(orderStatus);
            db.SaveChanges();
            dataGridView1.Refresh();
        }

        private void buttonOrderStatusEdit_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                OrderStatus orderStatus = dataGridView1.SelectedRows[0].DataBoundItem as OrderStatus;
                OrderStatusCreateUpdate dialog = new OrderStatusCreateUpdate();

                dialog.textBoxName.Text = orderStatus.Title;

                DialogResult result = dialog.ShowDialog();

                if (result == DialogResult.Cancel)
                    return;

                orderStatus.Title = dialog.textBoxName.Text;
                db.SaveChanges();
                dataGridView1.Refresh();
            }
        }

        private void OrderStatuss_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void buttonOrderStatusDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                OrderStatus orderStatus = dataGridView1.SelectedRows[0].DataBoundItem as OrderStatus;
                db.OrderStatuses.Remove(orderStatus);
                db.SaveChanges();
                dataGridView1.Refresh();
            }
        }
    }
}
