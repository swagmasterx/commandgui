﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommandProject.DataBaseEntities;

namespace CommandProject
{
    public partial class TaskTypes : Form
    {
        Context db;
        public TaskTypes()
        {
            InitializeComponent();
            db = new Context();
            db.TaskTypes.Load();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = db.TaskTypes.Local.ToBindingList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TaskTypeCreateUpdate dialog = new TaskTypeCreateUpdate();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.Cancel)
                return;

            TaskType taskType = new TaskType();
            taskType.Title = dialog.textBoxName.Text;

            db.TaskTypes.Add(taskType);
            db.SaveChanges();
            dataGridView1.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                TaskType taskType = dataGridView1.SelectedRows[0].DataBoundItem as TaskType;
                SubjectCreateUpdate dialog = new SubjectCreateUpdate();

                dialog.textBoxName.Text = taskType.Title;

                DialogResult result = dialog.ShowDialog();

                if (result == DialogResult.Cancel)
                    return;

                taskType.Title = dialog.textBoxName.Text;
                db.SaveChanges();
                dataGridView1.Refresh();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                TaskType taskType = dataGridView1.SelectedRows[0].DataBoundItem as TaskType;
                db.TaskTypes.Remove(taskType);
                db.SaveChanges();
                dataGridView1.Refresh();
            }
        }
    }
}
