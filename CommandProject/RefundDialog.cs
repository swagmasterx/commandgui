﻿using CommandProject.DataBaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CommandProject
{
    public partial class RefundDialog : Form
    {
        public DateTime Date;
        public string Reason;
        public int Cost;
        Context db;

        public RefundDialog()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (TryFillFields())
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Ошибка в данных.");
            }
        }

        private bool TryFillFields()
        {
            if (FieldsFilled())
            {
                Int32.TryParse(textBox2.Text, out Cost);
                Reason = textBox1.Text;
                Date = dateTimePicker1.Value;

                return true;
            }
            return false;
        }

        private bool FieldsFilled()
        {
            int tmp;
            return textBox2.Text.Length > 0 && Int32.TryParse(textBox2.Text, out tmp) && textBox1.Text.Length > 0;
        }
    }
}
