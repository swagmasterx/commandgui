﻿using CommandProject.DataBaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CommandProject
{
    public partial class ReworkDialog : Form
    {
        public DateTime fromDate, toDate;
        public ReworkReason Reason;
        Context db;


        public ReworkDialog(Context db)
        {
            InitializeComponent();
            this.db = db;

            comboBox1.DataSource = db.ReworkReasons.ToList();
            comboBox1.DisplayMember = "Title";
            comboBox1.ValueMember = "Id";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (TryFillFields())
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Выберите причину доработки.");
            }

        }

        private bool TryFillFields()
        {
            if (FieldsFilled())
            {
                fromDate = dateTimePicker1.Value;
                toDate = dateTimePicker2.Value;
                Reason = (ReworkReason)comboBox1.SelectedItem;
                return true;
            }
            return false;
        }

        private bool FieldsFilled()
        {
            return comboBox1.SelectedIndex != -1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
