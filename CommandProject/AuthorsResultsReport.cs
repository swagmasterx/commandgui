﻿using CommandProject.DataBaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CommandProject
{
    public partial class AuthorsResultsReport : Form
    {
        DateTime fromDate, toDate;
        Dictionary<int, DataBindingProjection> data;

        private class DataBindingProjection
        {
            public string Name {
                get
                {
                    return name + " " + surname + " " + fathername;
                }
            }
            public int Orders { get; set; }
            public int Reworks { get; set; }
            public int Refunds { get; set; }


            public string name;
            public string surname;
            public string fathername;
        
        }

        private void button1_Click(object sender, EventArgs e)
        {
            fromDate = dateTimePicker1.Value;
            toDate = dateTimePicker2.Value;
            RefreshTable();
        }

        private void RefreshTable()
        {
            data = new Dictionary<int, DataBindingProjection>();

            if (fromDate == null || toDate == null)
            {
                return;
            }

            using (var db = new Context())
            {
                var specialisations = db.Specialisations.Include("Author").Include("Orders").ToList();

                foreach (var spec in specialisations)
                {
                    var dataPortion = new DataBindingProjection { name = spec.Author.Name, surname = spec.Author.Surname, fathername = spec.Author.Fathername};
                    int authorId = spec.Author.Id;

                    var orders = db.Orders.Include("Specialisation").Include("Reworks").Include("RefundReason").Where(o => o.Specialisation.Id == spec.Id).ToList();
                    foreach (var ord in orders)
                    {
                        if(ord.CompletionDate >= fromDate && ord.CompletionDate <= toDate)
                        {
                            dataPortion.Orders++;
                            if (ord.RefundReason != null)
                                dataPortion.Refunds++;
                            dataPortion.Reworks += ord.Reworks.Count();
                        }
                    }

                    ApplyDataPortion(authorId, dataPortion);
                }

                dataGridView1.DataSource = data.Values.ToList();
                RefreshTableHeaders();
            }
        }

        private void RefreshTableHeaders()
        {
            dataGridView1.Columns[0].HeaderText = "Имя автора";
            dataGridView1.Columns[1].HeaderText = "Кол-во заказов";
            dataGridView1.Columns[2].HeaderText = "Кол-во доработок";
            dataGridView1.Columns[3].HeaderText = "Кол-во возвращений денег";
        }

        private void ApplyDataPortion(int authorId, DataBindingProjection dataPortion)
        {
            if (data.ContainsKey(authorId))
            {
                DataBindingProjection value;
                data.TryGetValue(authorId, out value);
                value.Orders += dataPortion.Orders;
                value.Refunds += dataPortion.Refunds;
                value.Reworks += dataPortion.Reworks;
            }
            else
            {
                data.Add(authorId, dataPortion);
            }
        }

        public AuthorsResultsReport()
        {
            InitializeComponent();
        }
    }
}
