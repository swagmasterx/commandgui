﻿namespace CommandProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.справочникиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.авторыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.репетиторыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.предметыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.заказыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.типыРаботToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.квалификацииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетыToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетОДоходахToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетОРаботахАвторовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.статусыЗаказовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.справочникиToolStripMenuItem,
            this.отчетыToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(229, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // справочникиToolStripMenuItem
            // 
            this.справочникиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.авторыToolStripMenuItem,
            this.репетиторыToolStripMenuItem,
            this.предметыToolStripMenuItem,
            this.заказыToolStripMenuItem,
            this.типыРаботToolStripMenuItem,
            this.квалификацииToolStripMenuItem,
            this.статусыЗаказовToolStripMenuItem});
            this.справочникиToolStripMenuItem.Name = "справочникиToolStripMenuItem";
            this.справочникиToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.справочникиToolStripMenuItem.Text = "Навигация";
            // 
            // авторыToolStripMenuItem
            // 
            this.авторыToolStripMenuItem.Name = "авторыToolStripMenuItem";
            this.авторыToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.авторыToolStripMenuItem.Text = "Авторы";
            this.авторыToolStripMenuItem.Click += new System.EventHandler(this.авторыToolStripMenuItem_Click);
            // 
            // репетиторыToolStripMenuItem
            // 
            this.репетиторыToolStripMenuItem.Name = "репетиторыToolStripMenuItem";
            this.репетиторыToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.репетиторыToolStripMenuItem.Text = "Клиенты";
            this.репетиторыToolStripMenuItem.Click += new System.EventHandler(this.репетиторыToolStripMenuItem_Click);
            // 
            // предметыToolStripMenuItem
            // 
            this.предметыToolStripMenuItem.Name = "предметыToolStripMenuItem";
            this.предметыToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.предметыToolStripMenuItem.Text = "Области знания";
            this.предметыToolStripMenuItem.Click += new System.EventHandler(this.предметыToolStripMenuItem_Click);
            // 
            // заказыToolStripMenuItem
            // 
            this.заказыToolStripMenuItem.Name = "заказыToolStripMenuItem";
            this.заказыToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.заказыToolStripMenuItem.Text = "Заказы";
            this.заказыToolStripMenuItem.Click += new System.EventHandler(this.заказыToolStripMenuItem_Click);
            // 
            // типыРаботToolStripMenuItem
            // 
            this.типыРаботToolStripMenuItem.Name = "типыРаботToolStripMenuItem";
            this.типыРаботToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.типыРаботToolStripMenuItem.Text = "Типы работ";
            this.типыРаботToolStripMenuItem.Click += new System.EventHandler(this.типыРаботToolStripMenuItem_Click);
            // 
            // квалификацииToolStripMenuItem
            // 
            this.квалификацииToolStripMenuItem.Name = "квалификацииToolStripMenuItem";
            this.квалификацииToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.квалификацииToolStripMenuItem.Text = "Квалификации";
            this.квалификацииToolStripMenuItem.Click += new System.EventHandler(this.квалификацииToolStripMenuItem_Click);
            // 
            // отчетыToolStripMenuItem1
            // 
            this.отчетыToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.отчетОДоходахToolStripMenuItem,
            this.отчетОРаботахАвторовToolStripMenuItem});
            this.отчетыToolStripMenuItem1.Name = "отчетыToolStripMenuItem1";
            this.отчетыToolStripMenuItem1.Size = new System.Drawing.Size(60, 20);
            this.отчетыToolStripMenuItem1.Text = "Отчеты";
            // 
            // отчетОДоходахToolStripMenuItem
            // 
            this.отчетОДоходахToolStripMenuItem.Name = "отчетОДоходахToolStripMenuItem";
            this.отчетОДоходахToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.отчетОДоходахToolStripMenuItem.Text = "Отчет о доходах";
            this.отчетОДоходахToolStripMenuItem.Click += new System.EventHandler(this.отчетОДоходахToolStripMenuItem_Click);
            // 
            // отчетОРаботахАвторовToolStripMenuItem
            // 
            this.отчетОРаботахАвторовToolStripMenuItem.Name = "отчетОРаботахАвторовToolStripMenuItem";
            this.отчетОРаботахАвторовToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.отчетОРаботахАвторовToolStripMenuItem.Text = "Отчет о работах авторов";
            this.отчетОРаботахАвторовToolStripMenuItem.Click += new System.EventHandler(this.отчетОРаботахАвторовToolStripMenuItem_Click);
            // 
            // статусыЗаказовToolStripMenuItem
            // 
            this.статусыЗаказовToolStripMenuItem.Name = "статусыЗаказовToolStripMenuItem";
            this.статусыЗаказовToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.статусыЗаказовToolStripMenuItem.Text = "Статусы заказов";
            this.статусыЗаказовToolStripMenuItem.Click += new System.EventHandler(this.статусыЗаказовToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(229, 204);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem справочникиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem авторыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem репетиторыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem предметыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem заказыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчетыToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem отчетОДоходахToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчетОРаботахАвторовToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem типыРаботToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem квалификацииToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem статусыЗаказовToolStripMenuItem;
    }
}

