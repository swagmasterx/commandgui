﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommandProject.DataBaseEntities;

namespace CommandProject
{
    public partial class Authors : Form
    {
        Context db;
        public Authors()
        {
            InitializeComponent();
            db = new Context();
            db.Authors.Load();
            db.TaskTypes.Load();
            db.Qualifications.Load();
            db.Subjects.Load();
            db.Specialisations.Load();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView2.AutoGenerateColumns = false;
            comboBox2.DataSource = db.TaskTypes.Local.ToBindingList();
            comboBox2.DisplayMember = "Title";
            comboBox1.DataSource = db.Subjects.Local.ToBindingList();
            comboBox1.DisplayMember = "Title";
            comboBox3.DataSource = db.Qualifications.Local.ToBindingList();
            comboBox3.DisplayMember = "Title";
            dataGridView1.DataSource = db.Authors.Local.ToBindingList();
        }


        private void Authors_Load(object sender, EventArgs e)
        {
            ResetFields();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ResetFields();
            dataGridView1.DataSource = db.Authors.Local.ToBindingList();
        }

        private void ResetFields()
        {
            comboBox1.Text = "";
            comboBox2.Text = "";
            comboBox3.Text = "";
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
        }

        private void buttonAuthorAdd_Click(object sender, EventArgs e)
        {
            AuthorCreateUpdate dialog = new AuthorCreateUpdate(db);
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.Cancel)
                return;

            Author author = new Author();
            author.Name = dialog.textBox1.Text;
            author.Surname = dialog.textBox2.Text;
            author.Fathername = dialog.textBox3.Text;
            author.Qualification = dialog.comboBox1.SelectedItem as Qualification;
            author.Blocked = dialog.checkBox1.Checked;

            db.Authors.Add(author);
            db.SaveChanges();
            dataGridView1.Refresh();
        }

        private void buttonAddSpecialisation_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Author author = dataGridView1.SelectedRows[0].DataBoundItem as Author;
                SpecialisationCreateUpdate dialog = new SpecialisationCreateUpdate();
                dialog.comboBox1.DataSource = db.TaskTypes.Local.ToBindingList();
                dialog.comboBox1.DisplayMember = "Title";
                dialog.comboBox2.DataSource = db.Subjects.Local.ToBindingList();
                dialog.comboBox2.DisplayMember = "Title";
                dialog.label1.Text += author.Name + " " + author.Surname;
                DialogResult result = dialog.ShowDialog();

                if (result == DialogResult.Cancel)
                    return;

                Specialisation specialisation = new Specialisation();
                specialisation.Author = author;
                specialisation.TaskType = dialog.comboBox1.SelectedItem as TaskType;
                specialisation.Subject = dialog.comboBox2.SelectedItem as Subject;
                specialisation.SalaryFrom = Int32.Parse(dialog.textBox1.Text);
                specialisation.SalaryTo = Int32.Parse(dialog.textBox2.Text);

                db.Specialisations.Add(specialisation);
                db.SaveChanges();
                BindSpecializations();
                dataGridView2.Refresh();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void авторыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new Authors()).Show();
        }

        private void репетиторыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new Clients()).Show();
        }

        private void предметыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new Subjects()).Show();
        }

        private void отчетыToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
        }

        private void заказыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new Orders()).Show();
        }

        private void авторыToolStripMenuItem_Click_1(object sender, EventArgs e)
        {

        }

        private void buttonAuthorUpdate_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Author author = dataGridView1.SelectedRows[0].DataBoundItem as Author;
                AuthorCreateUpdate dialog = new AuthorCreateUpdate(db);

                dialog.textBox1.Text = author.Name;
                dialog.textBox2.Text = author.Surname;
                dialog.textBox3.Text = author.Fathername;
                dialog.comboBox1.SelectedItem = author.Qualification;
                dialog.checkBox1.Checked = author.Blocked;

                DialogResult result = dialog.ShowDialog();

                if (result == DialogResult.Cancel)
                    return;

                author.Name = dialog.textBox1.Text;
                author.Surname = dialog.textBox2.Text;
                author.Fathername = dialog.textBox3.Text;
                author.Qualification = dialog.comboBox1.SelectedItem as Qualification;
                author.Blocked = dialog.checkBox1.Checked;

                db.SaveChanges();
                dataGridView1.Refresh();
            }
        }

        private void buttonAuthorDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Author author = dataGridView1.SelectedRows[0].DataBoundItem as Author;
                db.Authors.Remove(author);
                db.SaveChanges();
                dataGridView1.Refresh();
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
                BindSpecializations();
            else
                dataGridView2.DataSource = null;
        }

        private void BindSpecializations()
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Author author = dataGridView1.SelectedRows[0].DataBoundItem as Author;
                dataGridView2.DataSource = db.Specialisations.Local.Where(s => s.Author.Equals(author)).ToList();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                if (dataGridView2.SelectedRows.Count > 0)
                {
                    Author author = dataGridView1.SelectedRows[0].DataBoundItem as Author;
                    Specialisation specialization = dataGridView2.SelectedRows[0].DataBoundItem as Specialisation;
                    SpecialisationCreateUpdate dialog = new SpecialisationCreateUpdate();
                    dialog.comboBox1.DataSource = db.TaskTypes.Local.ToBindingList();
                    dialog.comboBox1.DisplayMember = "Title";
                    dialog.comboBox2.DataSource = db.Subjects.Local.ToBindingList();
                    dialog.comboBox2.DisplayMember = "Title";
                    dialog.label1.Text += author.Name + " " + author.Surname;
                    dialog.comboBox1.SelectedItem = specialization.TaskType;
                    dialog.comboBox2.SelectedItem = specialization.Subject;
                    dialog.textBox1.Text = specialization.SalaryFrom.ToString();
                    dialog.textBox2.Text = specialization.SalaryTo.ToString();

                    DialogResult result = dialog.ShowDialog();

                    if (result == DialogResult.Cancel)
                        return;

                    specialization.TaskType = dialog.comboBox1.SelectedItem as TaskType;
                    specialization.Subject = dialog.comboBox2.SelectedItem as Subject;
                    specialization.SalaryFrom = Int32.Parse(dialog.textBox1.Text);
                    specialization.SalaryTo = Int32.Parse(dialog.textBox2.Text);
                    
                    db.SaveChanges();
                    dataGridView2.Refresh();
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                if (dataGridView2.SelectedRows.Count > 0)
                {
                    Specialisation specialization = dataGridView2.SelectedRows[0].DataBoundItem as Specialisation;

                    db.Specialisations.Remove(specialization);
                    db.SaveChanges();
                    BindSpecializations();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var query = db.Authors.Local.Where
            (
                a =>
                (textBox1.Text.Length == 0 || textBox1.Text == a.Name) &&
                (textBox2.Text.Length == 0 || textBox2.Text == a.Surname) &&
                (textBox3.Text.Length == 0 || textBox3.Text == a.Fathername) &&
                (comboBox3.Text.Length == 0 || comboBox3.SelectedItem == a.Qualification) &&
                a.Specialisations.Where
                (
                    s =>
                    (comboBox1.Text.Length == 0 || comboBox1.SelectedItem == s.Subject) &&
                    (comboBox2.Text.Length == 0 || comboBox2.SelectedItem == s.TaskType) && 
                    (textBox4.Text.Length == 0 || !(s.SalaryTo < Int32.Parse(textBox4.Text))) &&
                    (textBox5.Text.Length == 0 || !(s.SalaryFrom > Int32.Parse(textBox5.Text)))
                ).Count() != 0
            );


            dataGridView1.DataSource = query.ToList();
        }
    }
}
