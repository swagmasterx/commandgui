﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommandProject.DataBaseEntities;

namespace CommandProject
{
    public partial class Qualifications : Form
    {
        Context db;
        public Qualifications()
        {
            InitializeComponent();
            db = new Context();
            db.Qualifications.Load();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = db.Qualifications.Local.ToBindingList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            QualificationCreateUpdate dialog = new QualificationCreateUpdate();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.Cancel)
                return;

            Qualification qualification = new Qualification();
            qualification.Title = dialog.textBoxName.Text;

            db.Qualifications.Add(qualification);
            db.SaveChanges();
            dataGridView1.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Qualification qualification = dataGridView1.SelectedRows[0].DataBoundItem as Qualification;
                SubjectCreateUpdate dialog = new SubjectCreateUpdate();

                dialog.textBoxName.Text = qualification.Title;

                DialogResult result = dialog.ShowDialog();

                if (result == DialogResult.Cancel)
                    return;

                qualification.Title = dialog.textBoxName.Text;
                db.SaveChanges();
                dataGridView1.Refresh();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Qualification qualification = dataGridView1.SelectedRows[0].DataBoundItem as Qualification;
                db.Qualifications.Remove(qualification);
                db.SaveChanges();
                dataGridView1.Refresh();
            }
        }
    }
}
