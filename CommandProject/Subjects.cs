﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommandProject.DataBaseEntities;

namespace CommandProject
{
    public partial class Subjects : Form
    {
        Context db;
        public Subjects()
        {
            InitializeComponent();

            db = new Context();
            
            db.Subjects.Load();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = db.Subjects.Local.ToBindingList();
        }

        private void buttonSubjectAdd_Click(object sender, EventArgs e)
        {
            SubjectCreateUpdate dialog = new SubjectCreateUpdate();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.Cancel)
                return;

            Subject subject = new Subject();
            subject.Title = dialog.textBoxName.Text;

            db.Subjects.Add(subject);
            db.SaveChanges();
            dataGridView1.Refresh();
        }

        private void buttonSubjectEdit_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Subject subject = dataGridView1.SelectedRows[0].DataBoundItem as Subject;
                SubjectCreateUpdate dialog = new SubjectCreateUpdate();

                dialog.textBoxName.Text = subject.Title;

                DialogResult result = dialog.ShowDialog();

                if (result == DialogResult.Cancel)
                    return;

                subject.Title = dialog.textBoxName.Text;
                db.SaveChanges();
                dataGridView1.Refresh();
            }
        }

        private void Subjects_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void buttonSubjectDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Subject subject = dataGridView1.SelectedRows[0].DataBoundItem as Subject;
                db.Subjects.Remove(subject);
                db.SaveChanges();
                dataGridView1.Refresh();
            }
        }
    }
}
