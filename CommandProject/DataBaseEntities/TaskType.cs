﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandProject.DataBaseEntities
{
    public class TaskType
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public virtual ICollection<Specialisation> Specialisations { get; set; }
    }
}
