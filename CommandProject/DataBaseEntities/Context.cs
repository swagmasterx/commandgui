﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandProject.DataBaseEntities
{
    public class Context: DbContext
    {
        public Context() : base("name = PostgresDB") { }

        public DbSet<Author> Authors { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<Qualification> Qualifications { get; set; }
        public DbSet<Refund> Refunds { get; set; }
        public DbSet<Rework> Reworks { get; set; }
        public DbSet<ReworkReason> ReworkReasons { get; set; }
        public DbSet<Specialisation> Specialisations { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<TaskType> TaskTypes { get; set; }
    }
}
