﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandProject.DataBaseEntities
{
    public class Client
    {
        public int Id { get; set; }
        public string Name {get; set;}
        public string Surname { get; set; }
        public string Fathername { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool Blocked { get; set; }

        public string WholeName
        {
            get { return Name + " " + Surname; }
        }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
