﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandProject.DataBaseEntities
{
    public class Refund
    {
        [Key]
        [ForeignKey("Order")]
        public int Id { get; set; }
        public string Reason { get; set; }
        public int Cost { get; set; }
        public DateTime Date { get; set; }

        [Required]
        public virtual Order Order { get; set; }
    }
}
