﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace CommandProject.DataBaseEntities
{
    public class Specialisation
    {
        public int Id { get; set; }
        public int SalaryFrom { get; set; }
        public int SalaryTo { get; set; }

        [Required]
        public virtual Author Author { get; set; }
        [Required]
        public virtual Subject Subject { get; set; }
        [Required]
        public virtual TaskType TaskType { get; set; }

        public string SubjectTitle
        {
            get
            {
                if (Subject != null)
                    return Subject.Title;
                else
                    return null;
            }
        }

        public string TaskTypeTitle
        {
            get
            {
                if (TaskType != null)
                    return TaskType.Title;
                else
                    return null;
            }
        }

        public string WholeSpecialization
        {
            get
            {
                if (Author == null)
                    return null;
                if (Subject == null)
                    return null;
                if (TaskType == null)
                    return null;

                return Author.Name + " " + Author.Surname + " - " + Subject.Title + " - " + TaskType.Title;
            }
        }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
