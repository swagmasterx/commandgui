﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandProject.DataBaseEntities
{
    public class Rework
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        [Required]
        public virtual ReworkReason ReworkReason { get; set; }
        [Required]
        public virtual Order Order { get; set; }
    }
}
