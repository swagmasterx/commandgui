﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace CommandProject.DataBaseEntities
{
    public class Order
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CompletionDate { get; set; }
        public string Task { get; set; }
        public int Price { get; set; }
        public int AuthorSalary { get; set; }

        public string AuthorName
        {
            get {
                if (Specialisation == null)
                    return null;
                if (Specialisation.Author == null)
                    return null;
                return Specialisation.Author.Name + " " + Specialisation.Author.Surname;
            }
        }

        public string ClientName
        {
            get {
                if (Client == null)
                    return null;
                return Client.Name + " " + Client.Surname;
            }
        }

        public string OrderStatusTitle
        {
            get {
                if (OrderStatus == null)
                    return null;
                return OrderStatus.Title; }
        }

        public string SubjectTitle
        {
            get {
                if (Specialisation == null)
                    return null;
                if (Specialisation.Subject == null)
                    return null;
                return Specialisation.Subject.Title; }
        }

        public string TaskTypeTitle
        {
            get {
                if (Specialisation == null)
                    return null;
                if (Specialisation.TaskType == null)
                    return null;
                return Specialisation.TaskType.Title; }
        }

        [Required]
        public virtual Client Client { get; set; }
        public virtual Refund RefundReason { get; set; }
        public virtual Specialisation Specialisation { get; set; }
        public virtual OrderStatus OrderStatus { get; set; }

        public virtual ICollection<Rework> Reworks { get; set; }
    }
}
